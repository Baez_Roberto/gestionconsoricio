-- Inserta super usuarios a tabla de usuarios
insert into administradores (login,password,nombre_administrador,telefono_administrador,correo_administrador)
values('baezmr','roberto123','Baez Roberto',154027867,'mariorobertobaez@gmail.com');

insert into administradores (login,password,nombre_administrador,telefono_administrador,correo_administrador)
values('campuzanocj','cinthia123','Campuzano Cinthia',154819480,'campuzanocinthialujan@gmail.com');

insert into administradores (login,password,nombre_administrador,telefono_administrador,correo_administrador)
values('fernandezyg','gonzalo123','Fernandez Yaique Gonzalo',154585870,'fygonzalo@gmail.com');

-- Inserta empresas de ejemplo
insert into empresa (idempresa, nombreEmpresa,direccion,telefono)values(1,'Proyecto Final','French 414', 3624027867);
insert into empresa (idempresa, nombreEmpresa,direccion,telefono)values(2,'Juan Pablo Prueba','Calle Wallaby, 42, Sidney', 3624666666);


-- Inserta roles por Usuario
insert into consorcios_x_administradores (idempresa,login) values (1,'baezmr');
insert into consorcios_x_administradores (idempresa,login) values (1,,'campuzanocj');
insert into consorcios_x_administradores (idempresa,login) values (1,'fernandezyg');
insert into consorcios_x_administradores (idempresa,login) values (2,'baezmr');

