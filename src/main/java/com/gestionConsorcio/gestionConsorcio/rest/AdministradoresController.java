package com.gestionConsorcio.gestionConsorcio.rest;


import com.gestionConsorcio.gestionConsorcio.dao.AdministradoresRepository;
import com.gestionConsorcio.gestionConsorcio.model.Administradores;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE})
public class AdministradoresController {

    @Autowired
    private AdministradoresRepository repository;

    @GetMapping(path = "/administradores")
    @PreAuthorize("hasRole('SUPERUSUARIO')")
    public List<Administradores> getAdministradores(){
       return repository.findAll();
    }

    @GetMapping (path = "/administradorByNombre/{nombre}" )
    @PreAuthorize("hasRole('SUPERUSUARIO')")
    public List<Administradores> getAdministradoresByNombre (@PathVariable("nombre") String nombre){
        return repository.findAdministradoresByNombreContainingIgnoreCase(nombre);
    }

    @GetMapping (path = "/administradorByEmail/{email}")
    @PreAuthorize("hasRole('SUPERUSUARIO')")
    public Administradores getAdministradoresByEmail (@PathVariable ("email") String email){
        return repository.findByCorreoContainingIgnoreCase(email);
    }

    @GetMapping (path = "/administradorByTelefono/{telefono}")
    @PreAuthorize("hasRole('SUPERUSUARIO')")
    public List<Administradores> getAdministradoresByTelefono (@PathVariable ("telefono") Long telefono){
        return repository.findByTelefono(telefono);
    }

    @PostMapping (path = "/persistirAdministrador")
    @PreAuthorize("hasRole('SUPERUSUARIO')")
    public Administradores persitirRol(@RequestBody Administradores usuario   ) {
        //Administradores administrador_existente = repository.findByCorreoContainingIgnoreCase(usuario.getCorreo());
        //if (administrador_existente != null) {
        //    return null;
        //}
        return repository.save(usuario);
    }

    @PutMapping (path = "/bloquearAdministrador/{idadmin}")
    @PreAuthorize("hasRole('SUPERUSUARIO')")
    public String bloquearAdministrador (@PathVariable("idadmin") Integer id){
        if(repository.exists(id)){
            Administradores administrador = repository.findByidadministrador(id);
            administrador.setActivo(!administrador.getActivo());
            repository.save(administrador);
            return "Estado de administrador cambiado correctamente";
        }
        return "No existe el ID indicado";
    }








}
