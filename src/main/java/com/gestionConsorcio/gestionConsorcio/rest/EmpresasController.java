package com.gestionConsorcio.gestionConsorcio.rest;


import com.gestionConsorcio.gestionConsorcio.dao.EmpresasRepository;
import com.gestionConsorcio.gestionConsorcio.model.Empresas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmpresasController {


    @Autowired
    private EmpresasRepository repository;


    @GetMapping(path = "/empresas/id/{id_empresa}")
    @PreAuthorize("hasRole('SUPERUSUARIO')")
    public List<Empresas> getEmpresaId(@PathVariable("id_empresa") Long id){
        return repository.findByIdempresa(id);

    }

    @GetMapping(path = "/empresas")
    @PreAuthorize("hasRole('SUPERUSUARIO')")
    public List<Empresas> getEmpresas(){
        return repository.findAll();
    }


    @GetMapping (path = "/empresasByNombre/{nombre}" )
    @PreAuthorize("hasRole('SUPERUSUARIO')")
    public List<Empresas> getEmpresasByNombre (@PathVariable ("nombre") String nombre){
        return repository.findByNombreEmpresaIgnoreCase(nombre);
    }


    @GetMapping (path = "/empresasByTelefono/{telefono}")
    @PreAuthorize("hasRole('SUPERUSUARIO')")
    public List<Empresas> getEmpresasByTelefono (@PathVariable ("telefono") Long telefono){
        return repository.findByTelefono(telefono);
    }

    @PostMapping (path = "/persistirConsorcio")
    @PreAuthorize("hasRole('SUPERUSUARIO')")
    public Empresas persitirConsorcio(@RequestBody Empresas consorcio) {
        return repository.save(consorcio);
    }

    @DeleteMapping (path = "/borrarConsorcio/{id}")
    @PreAuthorize("hasRole('SUPERUSUARIO')")
    public String borrarConsorcio (@PathVariable("id") Integer id){
        if(repository.exists(id)){
            repository.delete(id);
            if (repository.exists(id)){
                return "Error, no se pudo eliminar el registro";
            }
            return "Registro Borrado Correctamente";
        }
        return "No existe el ID indicado";
    }

}
