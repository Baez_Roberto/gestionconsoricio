package com.gestionConsorcio.gestionConsorcio.rest;

import com.gestionConsorcio.gestionConsorcio.model.IUser;
import com.gestionConsorcio.gestionConsorcio.services.UserService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/cuenta")
public class CuentaController {

    @GetMapping("/perfil")
    Perfil getPerfil(@AuthenticationPrincipal UserService.User user) {
        Perfil perfil = new Perfil();
        perfil.nombre = user.getInstance().getNombre();
        Collection<? extends GrantedAuthority> roles = user.getAuthorities();
        for (GrantedAuthority ga: roles) {
            perfil.roles.add(ga.toString());
        }
        return perfil;
    }

    public static final class Perfil {
        public String nombre;
        public Collection<String> roles;

        public Perfil() {
            this.roles = new ArrayList<>();
        }
    }

    @GetMapping("/login")
    void getLogin() {

    }
}
