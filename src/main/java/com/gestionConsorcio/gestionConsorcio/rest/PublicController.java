package com.gestionConsorcio.gestionConsorcio.rest;

import com.gestionConsorcio.gestionConsorcio.dao.UsuarioRepository;
import com.gestionConsorcio.gestionConsorcio.model.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(("/public"))
public class PublicController {

    private UsuarioRepository usuarioRepository;

    @Autowired
    public PublicController(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    @PostMapping("/nuevo_usuario")
    Usuario persistirUsuario(@RequestBody Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    @RequestMapping("/usuarios")
    List<Usuario> listado() {
        return usuarioRepository.findAll();
    }
}
