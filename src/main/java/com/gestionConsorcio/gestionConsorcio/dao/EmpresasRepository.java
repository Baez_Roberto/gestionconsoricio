package com.gestionConsorcio.gestionConsorcio.dao;

import com.gestionConsorcio.gestionConsorcio.model.Empresas;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmpresasRepository extends JpaRepository<Empresas, Integer> {

    List<Empresas> findByIdempresa(Long idempresa);
    List<Empresas> findByNombreEmpresaIgnoreCase(String nombreEmpresa);
    List<Empresas> findByTelefono (Long telefono);

}
