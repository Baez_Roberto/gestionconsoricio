package com.gestionConsorcio.gestionConsorcio.dao;

import com.gestionConsorcio.gestionConsorcio.model.Administradores;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface AdministradoresRepository extends JpaRepository<Administradores, Integer> {

    Administradores findByidadministrador(Integer idAdministrador);

    List<Administradores> findByCorreoAndPassword(String login, String password);

    List<Administradores> findAdministradoresByNombreContainingIgnoreCase (String nombre);

    List<Administradores> findByTelefono (Long telefono);

    Administradores findByCorreoContainingIgnoreCase (String correo);

}
