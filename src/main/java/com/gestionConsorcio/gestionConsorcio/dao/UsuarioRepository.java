package com.gestionConsorcio.gestionConsorcio.dao;

import com.gestionConsorcio.gestionConsorcio.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RequestMapping;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
