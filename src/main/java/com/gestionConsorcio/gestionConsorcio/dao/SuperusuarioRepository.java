package com.gestionConsorcio.gestionConsorcio.dao;

import com.gestionConsorcio.gestionConsorcio.model.Superusuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SuperusuarioRepository extends JpaRepository<Superusuario, Long> {

    Superusuario findByCorreo(String email);

}
