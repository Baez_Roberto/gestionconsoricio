package com.gestionConsorcio.gestionConsorcio.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Superusuario implements Serializable, IUser {

    @Id
    @GeneratedValue
    private Long idSuperusuario;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    private String nombre;

    @Column(unique = true)
    private String correo;

    private String password;

    private Boolean activo = true;

    public Long getIdSuperusuario() {
        return idSuperusuario;
    }

    public void setIdSuperusuario(Long idSuperusuario) {
        this.idSuperusuario = idSuperusuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String email) {
        this.correo = email;
    }

    public String getPassword() {
        return password;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
