package com.gestionConsorcio.gestionConsorcio.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "administradores")
public class Administradores implements Serializable, IUser {

    @Id
    @GeneratedValue(strategy =GenerationType.IDENTITY)
    private Integer idadministrador;

    @Column(name = "nombre_administrador")
    private String nombre;

    @Column(name = "correo_administrador", unique = true)
    private String correo;

    @Column(name = "telefono_administrador")
    private  Long  telefono;

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    private String direccion;

    @Column(name = "password")
    private String password;

    private Boolean activo = true;

    @ManyToMany(cascade = {CascadeType.DETACH}, fetch = FetchType.EAGER)
    @JoinTable(name = "consorcio_x_administrador",
            joinColumns = {
                    @JoinColumn(name = "idadministrador", referencedColumnName = "idadministrador" ,  nullable = false, updatable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "idempresa", referencedColumnName = "idempresa")})
    private Set<Empresas> consorcios;


    public Integer getIdadministrador() {
        return idadministrador;
    }

    public void setIdadministrador(Integer idadministrador) {
        this.idadministrador = idadministrador;
    }


    public Long getTelefono() {
        return telefono;
    }

    public void setTelefono(Long telefono) {
        this.telefono = telefono;
    }

    public String getPassword() {
        return password;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Empresas> getConsorcios() {
        return consorcios;
    }

    public void setConsorcios(Set<Empresas> consorcios) {
        this.consorcios = consorcios;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Administradores (String nombre,String correo,Long telefono,String direccion, String password, Set<Empresas> consorcios, Boolean activo) {
        super();
        this.nombre = nombre;
        this.correo = correo;
        this.consorcios = consorcios;
        this.password=password;
        this.telefono=telefono;
        this.activo = activo;
        this.direccion = direccion;
    }

    public Administradores() {
        super();
    }
}
