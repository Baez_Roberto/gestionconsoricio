package com.gestionConsorcio.gestionConsorcio.model;

public interface IUser {

    String getCorreo();
    String getNombre();
    String getPassword();
    Boolean getActivo();
}
