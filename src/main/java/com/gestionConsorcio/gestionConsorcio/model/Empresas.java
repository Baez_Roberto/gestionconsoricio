package com.gestionConsorcio.gestionConsorcio.model;


import javax.persistence.*;

@Entity
@Table (name = "empresa" , schema = "bd_gestion_consorcios")
public class Empresas {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer idempresa;

    @Column(name = "nombreempresa")
    private String nombreEmpresa;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "telefono")
    private Long telefono;

    private Boolean activo = true;



    public Empresas() {
        super();
    }

    public Empresas (String nombre_empresa,String direccion, Long telefono, Boolean activo) {
        super();
        this.nombreEmpresa = nombre_empresa;
        this.direccion=direccion;
        this.telefono=telefono;
        this.activo = activo;

    }

    public Integer getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(Integer idempresa) {
        this.idempresa = idempresa;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Long getTelefono() {
        return telefono;
    }

    public void setTelefono(Long telefono) {
        this.telefono = telefono;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }
}
