package com.gestionConsorcio.gestionConsorcio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionConsorcioApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionConsorcioApplication.class, args);
	}

}
