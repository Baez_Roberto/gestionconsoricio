package com.gestionConsorcio.gestionConsorcio.services;

import com.gestionConsorcio.gestionConsorcio.dao.AdministradoresRepository;
import com.gestionConsorcio.gestionConsorcio.dao.SuperusuarioRepository;
import com.gestionConsorcio.gestionConsorcio.model.Administradores;
import com.gestionConsorcio.gestionConsorcio.model.IUser;
import com.gestionConsorcio.gestionConsorcio.model.Superusuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class UserService implements UserDetailsService {

    private SuperusuarioRepository superusuarioRepository;
    private AdministradoresRepository administradoresRepository;

    @Autowired
    UserService(SuperusuarioRepository superusuarioRepository, AdministradoresRepository administradoresRepository) {
        this.superusuarioRepository = superusuarioRepository;
        this.administradoresRepository = administradoresRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        int index_rol = login.lastIndexOf("/");
        String email = login.substring(0, index_rol);
        String rol = login.substring(index_rol + 1);

        switch (rol.toUpperCase()) {
            case "SUPERUSUARIO":
                Superusuario superusuario = superusuarioRepository.findByCorreo(email);
                if (superusuario == null) {
                    throw new UsernameNotFoundException("No se encontro el usuario");
                }
                return new User(superusuario, rol);
            case "ADMINISTRADOR":
                Administradores administradores = administradoresRepository.findByCorreoContainingIgnoreCase(email);
                if (administradores == null) {
                    throw new UsernameNotFoundException("No se encontro el usuario");
                }
                return new User(administradores, rol);
            default:
                throw new UsernameNotFoundException("No se pudo identificar el rol");
        }
    }

    public final static class User implements UserDetails {

        private IUser u;
        private String rol;

        User(IUser u, String rol) {
            this.u = u;
            this.rol = rol;
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + this.rol));
            return grantedAuthorities;
        }

        public IUser getInstance() {
            return u;
        }

        @Override
        public String getPassword() {
            return u.getPassword();
        }

        @Override
        public String getUsername() {
            return u.getCorreo();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return this.u.getActivo();
        }
    }
}
